// Diberikan sebuah function antrian yang menerima dua parameter yaitu line bertipe array dan person bertipe string. Function ini akan mengembalikan array yang dimana data array tersebut sudah bertambah diakhir array sesuai dengan parameter person.
// Panggil Antrian

function antrian(line, string) {
  var masuk = antri.splice(line, 0, string);
  console.log(antri);
}
var antri = ['Jojo', 'Caca', 'Kaka'];
console.log(antrian(1, 'Lala'));

// Diberikan sebuah function ganjilGenap yang menerima satu parameter plat bertipe string. Parameter plat berisi informasi kumpulan plat dimana nomor antar plat dipisahkan oleh titik koma(;).
// Function ini akan mengembalikan keterangan jumlah plat genap dan jumlah plat ganjil.

var ganjil = 0;
var genap = 0;
function ganjilGenap(plat) {
  if (plat == '') {
    return 'plat tidak ditemukan';
  } else if (plat == undefined) {
    return 'invalid data';
  } else if (plat !== '' || plat !== undefined) {
    var c = plat.split(';');
    for (var i = 0; i < c.length; i++) {
      if (c[i] % 2 == 1) {
        ganjil += 1;
      } else if (c[i] % 2 == 0) {
        genap += 1;
      }
    }

    if (ganjil == 0) {
      ganjil = 'plat ganjil tidak ditemukan';
    } else if (genap == 0) {
      genap = 'plat genap tidak ditemukan';
    }
    console.log('Plat genap sebanyak ' + genap + ' dan plat ganjil sebanyak ' + ganjil);
    ganjil = 0;
    genap = 0;
    c = 0;
  }
}
console.log(ganjilGenap('2341;3429;864;1309;1276')); //plat genap sebanyak 2 dan plat ganjil sebanyak 3
console.log(ganjilGenap('2347;3429;1305')); //plat ganjil sebanyak 3 dan plat genap tidak ditemukan
console.log(ganjilGenap('864;1308;1276;1432')); //plat genap sebanyak 4 dan plat ganjil tidak ditemukan
console.log(ganjilGenap('')); //plat tidak ditemukan
console.log(ganjilGenap()); //invalid data
